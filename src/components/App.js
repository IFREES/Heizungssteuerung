import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import HeizungsSteuerung from "./HeizungsSteuerung/HeizungsSteuerung";
import NavBar from "./navBar/navBar";
import Login from "./common/login";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <div className="App">
          <Switch>
            <Route
              path="/programmierung/sautter"
              render={props => <HeizungsSteuerung {...props} area={"areaSautter"} />}
            />
            <Route
              path="/programmierung/mieter"
              render={props => <HeizungsSteuerung {...props} area={"test"} />}
            />
            <Route path="/login" component={Login} />
            <Redirect exact from="/" to="/programmierung/sautter" />
          </Switch>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
